#!/bin/sh
set -e
if [ ! -z "${DEBUG}" ]; then
	set -x
fi

export GOPATH=/tmp
export HOME=/tmp
OUTFILE=/lambda/lambda.zip
SOURCES=/tmp/src/peaches.io/app

if [ -e ${OUTFILE} ]
then
	echo ERROR: Outfile $OUTFILE already exists -- refusing to overwrite.
	exit 1
fi

mkdir -p ${SOURCES}
cd ${SOURCES}
if [ ! -z "${S3_SOURCE}" ]
then
	echo "NOTICE: Downloading lambda sources from S3 at ${S3_SOURCE}"
	wget -O /tmp/lambda_src.zip -- "${S3_SOURCE}" && \
	unzip /tmp/lambda_src.zip
else
	mv /lambda/* .
fi

if [[ $(find ${SOURCES} -type f -name '*.go' | wc -l) == 0 ]]
then
	echo No source .go files found, make sure to mount /lambda where you have your source files. The resulting lambda.zip will be placed there.
	exit 1
fi

# install dependencies
go get .
# build our binary
env GOOS=linux GOARCH=amd64 go build -ldflags="-s -w" .
upx -9 app

# Optionally delete source .go files
if [ "${DELETESOURCE}" == "1" ]
then
	find . -name '*.go' -delete
fi

# Create ZIP file
zip -r9q ${OUTFILE} app
if [ ! -z "${S3_TARGET}" ]
then
	echo "NOTICE: Uploading final lambda bundle to S3 at ${S3_TARGET}"
	curl -vvv -D - -X PUT --upload-file "${OUTFILE}" "${S3_TARGET}"
	echo "Upload complete"
fi
echo Zip complete: `ls -la ${OUTFILE}`
